# 2022 2023 Dev Project



## 配置环境
* python环境 3.8.2
    * 可以参考 https://blog.csdn.net/czmjy/article/details/124374313 进行本地多版本管理
    * 使用python自带venv创建虚拟环境，方便库依赖版本管理，并和本地python环境隔离

使用命令` pip install -r ./requirements.txt`安装依赖库

## 运行
`python38 ./gesturecontrol.py `


